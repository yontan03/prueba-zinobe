<?php

require_once "app/models/User.php";
require_once "app/models/Country.php";

use App\Models\User; //Modelo Usuario
use App\Models\Country; //Modelo Pais
use Particle\Validator\Validator; //Libreria para validacion, Particle Validator
use Particle\Validator\Rule\LengthBetween; //Clase Validacion Min y Max
use Particle\Validator\Rule\NotEmpty; //Clase Validacion requerido
use Particle\Validator\Rule\Email; //Clase validacion email
use Particle\Validator\Rule\Regex;  //Clase validacion expresion regular
use Particle\Validator\Rule\Equal;  //Clase validacion datos iguales

#Clase Usuario
class userController{
    #Metodo Index, se carga por defecto
    public function index(){
        //Se llama la vista de login
        require_once "app/views/user/login.php";    
    }

    #Metodo Create, carga la vista para crear el usuario
    public function create(){

        $paises = Country::get(); //Consulta que devuelve listado de paises
        require_once "app/views/user/create.php";
    }  
    
    #Metodo View, carga la vista que lista los usuarios del sistema
    public function view(){

        $users = User::orderBy('id', 'desc')->get(); //Consulta que devuelve el listado de usuarios ordenado en forma descendente
        require_once "app/views/user/view.php";    
    }     

    #Metodo login, valida si los datos de acceso son correctos y accede al sistema        
    public function login(){

        if(isset($_POST['btn_submit'])){
            
            //Se valida si hay un post y se almacenan cada uno de los datos del form
            $usuario = isset($_POST['usuario']) ? $_POST['usuario'] : false;
            $pass = isset($_POST['pass']) ? $_POST['pass'] : false;

            $user = User::where("email","=",$usuario)->first(); //Consulta si el correo esta registrado en sistema
            $session = new Session(); //Crea una instancia de la clase Session
            $aux = false;
            if(!empty($user)){
                //Si la consulta devuelve un resultado, verificamos el pass y se accede al sistema
                //de lo contrario se envía un mensaje por pantalla
                $pass_verify = password_verify($pass,$user->password);

                if($pass_verify){
                    $aux = true;
                }
            }

            if($aux == false){
                $session->createSession($user,2);
                header("Location: ".base_url);
            }else{
                $session->createSession($user,1);
                header("Location: ".base_url."user/view");
            }
        }else{
            header("Location: ".base_url);
        }
      
    }   

    #Metodo save, hace el registro de un usuario si todos los datos son correctos
    public function save(){

        if(isset($_POST)){
            //Almanecenamiento de Datos
            $nombre = isset($_POST['nombre']) ? $_POST['nombre'] : false;
            $usuario = isset($_POST['usuario']) ? $_POST['usuario'] : false;
            $pais = isset($_POST['pais']) ? $_POST['pais'] : false;
            $pass = isset($_POST['pass']) ? $_POST['pass'] : false;
            $pass2 = isset($_POST['pass2']) ? $_POST['pass2'] : false;


            $values = [
                    'name' => $nombre,
                    'usuario' => $usuario,
                    'pais' => $pais,
                    'pass' => $pass,
                    'pass2' => $pass2,
            ];
            //Se crea una instancia de la clase para validacion de datos
            $v = new Validator;
            $v->required('name')->lengthBetween(3, 255);
            $v->required('usuario')->email();
            $v->required('pais');
            $v->required('pass')->lengthBetween(6, 255)->regex('(\d)');
            $v->required('pass2')->equals($pass);

            //Se personalizan mensajes de validacion 
            $v->overwriteMessages([
                'name' => [
                    LengthBetween::TOO_SHORT => 'Nombre debe tener minimo 3 caracteres',
                    LengthBetween::TOO_LONG => 'Nombre debe tener maximo 255 caracteres',
                    NotEmpty::EMPTY_VALUE => 'Nombre es un campo requerido'
                ],
                'usuario' => [
                    Email::INVALID_FORMAT => 'Formato email incorrecto',
                    NotEmpty::EMPTY_VALUE => 'Correo es un campo requerido'
                ],
                'pais' => [
                    NotEmpty::EMPTY_VALUE => 'Pais es un campo requerido'
                ],
                'pass' => [
                    Regex::NO_MATCH => 'Contraseña debe tener al menos un digito',
                    LengthBetween::TOO_SHORT => 'Contraseña debe tener minimo 6 caracteres',
                    LengthBetween::TOO_LONG => 'Contraseña debe tener maximo 255 caracteres',
                    NotEmpty::EMPTY_VALUE => 'Contraseña es un campo requerido'
                ],
                'pass2' => [
                    Equal::NOT_EQUAL => 'Las contraseñas no coinciden',
                    NotEmpty::EMPTY_VALUE => 'Contraseña es un campo requerido'
                ]
            ]);

            $result = $v->validate($values);

            $valid = $result->isValid(); 
            $array_message = $result->getMessages();
            $array_session = array();
            $obj = "";
            $session = new Session();
            
            //Validamos los datos, si hay errores se crea un array para visualizarlos por pantalla
            if(!$valid){
                foreach ($array_message as $key => $value){
                        foreach ($value as $fila) {
                            $array_session[$key] = $fila; 
                        }
                        
                    }

                    $obj = (object) $array_session;                    
                    $session->createSession($obj, 3);
                    header("Location: ".base_url."user/create"); 
              
            }else{
                //Se valida que el correo no exista en la BD
                $con_email = User::where("email","=",$usuario)->first();
                
                if(!empty($con_email)){
                     $array_session = array(
                        'usuario' => 'Este correo ya se encuentra registrado'
                    );
                    $obj = (object) $array_session; 
                    $session->createSession($obj, 3);
                    header("Location: ".base_url."user/create"); 
                }else{             
                //Una vez todos los datos sean validados correctamente, se crea una instancia del Modelo usuario y se guardan los datos en la BD.    
                $user = new User();       
                $user->name = $nombre;
                $user->email = $usuario;
                $user->country_id = $pais;
                $user->password = password_hash($pass, PASSWORD_BCRYPT, ['cost'>=4]);
                $user->save();
                $session->createSession($user, 1);
                header("Location: ".base_url."user/view");
                }
            }            
            
        }
           

    }
    #Metodo Logout, finaliza la session de usuario actual
    public function logout(){

        $session = new Session();
        $session->deleteSession();
        header("Location: ".base_url); 

    }  

}

?>