<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class User extends Model{

    protected $table = 'users';

    public function country(){

        return $this->belongsTo('App\Models\Country','country_id');
    }

}

