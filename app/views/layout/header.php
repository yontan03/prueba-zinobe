<?php
$session = new Session();
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Boostrap Css -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="<?=base_url?>app/assets/css/styles.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <title>Sistema de Usuarios</title>
</head>
<body>
    <!-- Modal-->
    <div class="modal" id="myModalWarning" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content panel-warning">
        <div class="modal-header alert-warning">
            <h5 class="modal-title">Advertencia</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body text-center" id="myModalWarningBody">

        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        </div>
        </div>
    </div>
    </div>
    <!--End Modal -->
    <header id="header">
        <div id="nav">
            <?php if (isset($_SESSION['usuario'])) :?>
            <ul>
                <a href="<?=base_url?>user/logout"><i class="fas fa-power-off fa-2x"></i></a>
                <?= 'Hola, '.$_SESSION['usuario']->name ?> 
            </ul>
            <?php endif ?>
        </div>  
    </header>
    <div class="containter">
        <div class="row">
            