<?php 
$session = new Session();
if($session->checkSession()){
    header("Location: ".base_url."user/view");
}

?>
    <div class="col-md-6 offset-md-3">
                <div class="mt-4 mb-2 border-bottom text-center">
                    <h2>Panel de  Acceso</h2>
                </div>
                <div class="alert alert-info text-center">
                    Bienvenidos al Sistema
                </div>
   
                <div class="card">
                    <div class="card-header">
                        Ingrese sus datos para tener acceso
                    </div>
                    <div class="card-body">
                        <form role="form" action="<?=base_url?>user/login" method="POST" id="formlogin" class="form-horizontal">
                            <div class="form-group row">
                              <label for="usuario" class="col-md-3 control-label text-right">Email</label> 
                              <div class="col-md-7">
                                  <input type="email" name="usuario" id="usuario" class="form-control" autocomplete="off" required>
                              </div> 
                            </div>

                            <div class="form-group row">
                              <label for="pass" class="col-md-3 control-label text-right">Contraseña</label> 
                              <div class="col-md-7">
                                  <input type="password" name="pass" id="pass" class="form-control" required>
                                    <?php if(isset($_SESSION['error_login'])) :?>
                                        <small style = "color:red;"><?=$_SESSION['error_login']?></small> 
                                    <?php endif ?> 
                              </div>
                            </div>
                                
                                
                            <div class="form-group row">
                                <div class="offset-md-1 col-md-9  text-right">
                                    <button type="submit" name="btn_submit" value="Enviar" class="btn btn-primary">Ingresar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <a href="<?=base_url?>user/create">Registro de Usuarios</a> | 
                <a href="<?=base_url?>user/login">Acceso de Usuarios</a>
<?php
if(isset($_SESSION['error_login'])){
    $session->endSession('error_login');  
}              
?>
