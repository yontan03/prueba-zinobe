<?php
$session = new Session();
if($session->checkSession()){
  header("Location: ".base_url."user/view");
}
?>
<div class="col-md-6 offset-md-3">
    <div class="mt-4 mb-2 border-bottom text-center">
                    <h2>Panel de Usuario</h2>
                </div>
                <div class="alert alert-info text-center">
                        Creación de usuario
                </div>
   
                <div class="card">
                    <div class="card-header">
                        Ingrese sus datos para registrarse en el sistema
                    </div>
                    <div class="card-body">
                        <form role="form" action="<?=base_url?>user/save" method="POST" id="formlogin" class="form-horizontal">
                            <div class="form-group row">
                              <label for="nombre" class="col-md-3 control-label text-right">Nombre</label> 
                              <div class="col-md-7">
                                  <input type="text" name="nombre" id="nombre" class="form-control" autocomplete="off" required>
                                  <?php if(isset($_SESSION['error_validacion']->name)) :?>
                                    <small style = "color:red;"><?=$_SESSION['error_validacion']->name?></small>
                                  <?php endif ?>
                              </div> 
                            </div>                        
                            <div class="form-group row">
                              <label for="usuario" class="col-md-3 control-label text-right">Email</label> 
                              <div class="col-md-7">
                                  <input type="text" name="usuario" id="usuario" class="form-control" autocomplete="off" required>
                                  <?php if(isset($_SESSION['error_validacion']->usuario)) :?>
                                    <small style = "color:red;"><?=$_SESSION['error_validacion']->usuario?></small>
                                  <?php endif ?>
                              </div> 
                            </div>
                            <div class="form-group row">
                              <label for="pais" class="col-md-3 control-label text-right">País</label> 
                              <div class="col-md-7">
                                  <select name="pais" id="pais" class="form-control" required>
                                      <option value="">Seleccione un Pais</option>
                                      <?php foreach ($paises as $pais) : ?>
                                      <option value="<?=$pais['id']?>"><?=$pais['country']?></option>
                                      <?php endforeach ?>
                                  </select>
                                  <?php if(isset($_SESSION['error_validacion']->pais)) :?>
                                    <small style = "color:red;"><?=$_SESSION['error_validacion']->pais?></small>
                                  <?php endif ?>
                              </div> 
                            </div>  
                            <div class="form-group row">
                              <label for="pass" class="col-md-3 control-label text-right">Contraseña</label> 
                              <div class="col-md-7">
                                  <input type="password" name="pass" id="pass" class="form-control" required>
                                  <?php if(isset($_SESSION['error_validacion']->pass)) :?>
                                    <small style = "color:red;"><?=$_SESSION['error_validacion']->pass?></small>
                                  <?php endif ?>
                              </div> 
                            </div>
                            <div class="form-group row">
                              <label for="pass2" class="col-md-3 control-label text-right">Confir Contraseña</label> 
                              <div class="col-md-7">
                                  <input type="password" name="pass2" id="pass2" class="form-control" required>
                                  <?php if(isset($_SESSION['error_validacion']->pass2)) :?>
                                    <small style = "color:red;"><?=$_SESSION['error_validacion']->pass2?></small>
                                  <?php endif ?>
                              </div> 
                            </div>
                             <div class="form-group row">
                                <div class="offset-md-1 col-md-9  text-right">
                                    <button type="submit" id="btn_submit" value="Enviar" class="btn btn-primary">Ingresar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>  
                <a href="<?=base_url?>user/create">Registro de Usuarios</a> | 
                <a href="<?=base_url?>user/login">Acceso de Usuarios</a>
<?php                     
if(isset($_SESSION['error_validacion'])){
    $session->endSession('error_validacion'); 
}  

?>        