<?php

use App\Models\User;

//Se valida que solo un usuario logueado pueda tener acceso al listado de usuarios
$session = new Session();
if(!$session->checkSession()){
  header("Location: ".base_url."user/index");
}

$users_ = $users;

//Se realiza la busqueda de usuarios por nombre y correo
if(isset($_POST['btn_buscar']) && (!empty(trim($_POST['busqueda'])))){
    $cadena = $_POST['busqueda'];
    $query = User::where("name","like",'%'.$cadena.'%')
                 ->orwhere("email","like",'%'.$cadena.'%')
                 ->orderBy('id', 'desc')            
                 ->get();
    $users_ = $query;
}


if(isset($_POST['btn_restaurar'])){
    $users_ = $users;
}
?>
    <div class="col-md-8 offset-md-2">
        <div class="mt-4 mb-2 border-bottom text-center">
                <h2>Listado de Usuarios</h2>
                </div>
   
                <div class="card mb-3">
                    <div class="card-header">
                        Listado de Usuarios del sistema
                    </div>
                    <div class="card-body">
                        
                            <form role="form" method="POST" id="formbuscar">
                                <div class="form-group row">
                                <div class="col-md-7">
                                    <input type="text" name="busqueda" id="busqueda" class="form-control" value="<?= isset($_POST['busqueda']) ? $_POST['busqueda'] : ''; ?>" autocomplete="off" required>
                                </div>  
                                <button type="submit" name="btn_buscar" class="btn btn-primary mb-4 mr-3">Buscar</button>
                                <?php if(isset($_POST['btn_buscar']) && (!empty(trim($_POST['busqueda'])))) : ?>
                                <button type="submit" name="btn_restaurar" class="btn btn-success mb-4">Restaurar</button>
                                <?php endif?>
                            </form>    
                        
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">Nombre</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">País</th>
                                    <th scope="col">Fecha de Registro</th>
                                </tr>
                            </thead>
                            <tbody id="contenido"> 
                                <?php foreach($users_ as $user) : ?>
                                <tr>
                                    <td><?=$user->name?></td>
                                    <td><?=$user->email?></td>
                                    <td><?=$user->country->country?></td>
                                    <td><?=$user->created_at?></td>
                                </tr>
                                <?php endforeach ?>

                            </tbody>
                        </table>
                    </div>
                </div> 

      