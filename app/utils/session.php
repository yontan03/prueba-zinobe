<?php

class Session{

    public $codSession = 'usuarios';

    public function __construct(){
        if(!isset($_SESSION)){
         session_name($this->codSession);
         session_start();
        }
    }

    public function checkSession(){

        $check = false;

        if(isset($_SESSION['usuario']) && !empty($_SESSION['usuario'])){
            $check = true;
        }

        return $check;
    }

    public function createSession(object $datos = null, $var){
        $_SESSION = array();

        if($var == 1){
            $_SESSION['usuario'] = $datos;
        }else if($var == 2){
            $_SESSION['error_login'] = 'Datos Incorrectos';
        }else if($var == 3){
            $_SESSION['error_validacion'] = $datos;
        }
        
        
    }

    public function deleteSession(){

        $_SESSION = array();

        if (ini_get("session.use_cookies")){
            $params = session_get_cookie_params();
            setcookie(session_name(), "", time() - 42000, $params["path"], $params["domain"], $params["secure"], $params["httponly"]);
        }

        session_destroy();

    }

    public static function endSession($name){
        if(isset($_SESSION[$name])){
        $_SESSION[$name] = NULL;
        unset($_SESSION[$name]);
        }
    }    

}

?>