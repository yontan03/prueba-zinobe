<?php

class App{

    private $url;

    public function __construct($urla){

            
            $this->url = $urla;

            //Verificamos si URL es vacía. De ser así se escoge controlador por defecto
            if(empty($this->url)){
                $controller = 'userController';
                $objeto = new $controller();
                $objeto->index();
                return false;
            }

            $url = rtrim($this->url, '/');
            $url = explode('/',$url);

            $fileController = 'app/controllers/'.$url[0].'Controller.php';
            $controller = $url[0].'Controller'; 

            //Se comprueba si existe controlador, y se crea una instancia
            if(file_exists($fileController)){
                
                $objeto = new $controller();
                //Se verifica si existe el metodo, de ser así se llama, de lo contrario se carga un metodo por defecto
                if(isset($url[1])){
                    $action = $url[1];
                    if(method_exists($controller,$url[1])){
                        $objeto->$action();
                    }else{
                        $controller = 'errorController';
                        $objeto = new $controller(); 
                    }                
                }else{
                    $objeto->index();
                }
            }else{
                $controller = 'errorController';
                $objeto = new $controller();                
            }

    }

}

?>