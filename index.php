<?php

require_once "vendor/autoload.php";
require_once "app/config/config.php";
require_once "app/app.php";
require_once "app/controllers/userController.php";
require_once "app/controllers/errorController.php";
require_once "app/utils/session.php";
require_once "app/utils/parameters.php";

require_once "app/views/layout/header.php";

$url = isset($_GET['controller']) ? $_GET['controller'] : NULL;
$app = new App($url);
            
                
require_once "app/views/layout/footer.php";


?>